package com.lagou.controller;

import com.lagou.pojo.Article;
import com.lagou.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


@Controller
public class ArticleController {

    private int pagesize=3;//初始化，每页3条

    private int pagenum = 0;//默认0开始的

    @Autowired
    private ArticleService articleService;

    @RequestMapping("/index")
    public String index(HttpServletRequest httpServletRequest,Model model){
        Pageable pageable=null;
        String page = httpServletRequest.getParameter("page");
        if(!StringUtils.isEmpty(page)){
            pageable= PageRequest.of(Integer.parseInt(page),pagesize);
        }else {
            pageable= PageRequest.of(pagenum,pagesize);
        }
        Page<Article> articlePage = articleService.findAll(pageable);
        model.addAttribute("articlePage",articlePage);
        return "index";
    }
}
