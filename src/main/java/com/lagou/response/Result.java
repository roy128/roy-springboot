package com.lagou.response;

import java.util.List;

public class Result<T> {
    public List<T> content;
    public long totalElements;
    public int totalPages;
    public int size;

}
