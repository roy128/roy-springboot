package com.lagou.repository;

import com.lagou.pojo.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.awt.print.Pageable;

/**
 *  jpa dao 层
 */
public interface ArticleRepository extends JpaRepository<Article,Integer>, CrudRepository<Article,Integer> {

}
